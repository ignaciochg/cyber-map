#!/usr/bin/env python3

import random
import argparse
import subprocess



# List source https://geekflare.com/real-time-cyber-attacks/
maps = {
   "DigitalAttackMap":{"description":
      """
      """,
      "url": "https://www.digitalattackmap.com/"},
   "FireEye":{"description":
      """
      """,
      "url": "https://www.fireeye.com/cyber-map/threat-map.html"},

   "Kaspersky":{"description":
      """
      """,
      "url": "https://cybermap.kaspersky.com/"},
   "ThreatCloud":{"description":
      """
      """,
      "url": "https://www.checkpoint.com/ThreatPortal/livemap.html"},
   "AKAMAI":{"description":
      """
      """,
      "url": "https://www.akamai.com/us/en/solutions/intelligent-platform/visualizing-akamai/real-time-web-monitor.jsp"},
   "Threatbutt":{"description":
      """
      """,
      "url": "https://threatbutt.com/map/"},
   "FortiGuard":{"description":
      """
      """,
      "url": "https://threatmap.fortiguard.com/"},
   "Bitdefender":{"description":
      """
      """,
      "url": "https://threatmap.bitdefender.com/"},

   "LookingGlass":{"description":
      """
      """,
      "url": "https://map.lookingglasscyber.com/"},
      

   "Talos":{"description":
      """
      """,
      "url": "https://talosintelligence.com/fullpage_maps/pulse"},
      
   "Netscout":{"description":
      """
      """,
      "url": "https://horizon.netscout.com/"}

}




def create_parser():
   parser = argparse.ArgumentParser(description='Launches Firefox with a Cyber Threat Map.')

   options = ["random"]+list(maps.keys())
   option_help_list = ', '.join(options)
   parser.add_argument('--map', metavar='MAP_NAME', type=str, dest='sel_map',
                       default="random", choices=["random"]+list(maps.keys()),
                       help=f'Select a map to launch (default: %(default)s) (options: {option_help_list})')

   return parser


if __name__ == '__main__':
   parser = create_parser()
   args = parser.parse_args()

   print(args.sel_map)
   if args.sel_map == "random":
      print("Random map selection... Enabled")
      selected_map_key = random.choice(list(maps.keys()))
   else:
      selected_map_key = args.sel_map

   print("Map selected:", selected_map_key)
   
   selected_map = maps[selected_map_key]

   program = "firefox"
   arguments = " "

   command = f'{program} {arguments} {selected_map["url"]}'

   print(f"Running: {command}")
   # subprocess.Popen(command)
   try:
      subprocess.run(command.split())
   except KeyboardInterrupt:
      print("Exiting...")

   # sleep(1000)
