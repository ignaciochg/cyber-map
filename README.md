# Cyber Maps
for displaying Cyber Threat Maps

# Usage 
```bash
wget -O - https://gitlab.com/ignaciochg/cyber-map/-/raw/master/cyber-map.py | python - 
```

or with arguemnts 
```bash
wget -O - https://gitlab.com/ignaciochg/cyber-map/-/raw/master/cyber-map.py | python - --map FireEye
```


